package pl.sda.registerofcitizens;

public class Citizen {

    //klasa POJO

    private String citizenID, firstName, lastName;

    public Citizen(String citizenID, String firstName, String lastName) {
        this.citizenID = citizenID;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getCitizenID() {
        return citizenID;
    }

    public void setCitizenID(String citizenID) {
        this.citizenID = citizenID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Citizen{" +
                "citizenID='" + citizenID + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
