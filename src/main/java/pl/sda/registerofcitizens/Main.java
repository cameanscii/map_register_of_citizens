package pl.sda.registerofcitizens;

public class Main {

    public static void main(String[] args) {
        RegisterOfCitizens registerOfCitizens=new RegisterOfCitizens();
        registerOfCitizens.addCitizen("91032101238","Marek","Kowalczyk");
        registerOfCitizens.addCitizen("99032101238","Marian","Kowalczyk2");
        registerOfCitizens.addCitizen("93032101238","Wiesiek","Kowalczyk3");
        registerOfCitizens.addCitizen("71032101238","Jan","Kowalczyk4");
        registerOfCitizens.addCitizen("31032101238","Zdzichu","Kowalczyk5");
        registerOfCitizens.addCitizen("77032101238","Edward","Kowalczyk");

        System.out.println(registerOfCitizens.findCitizensBornBefore(88));

        System.out.println(registerOfCitizens.findCitizensBornBeforeWithName(88,"Jan"));

        System.out.println(registerOfCitizens.findCitizensWithLastName("Kowalczyk"));

        System.out.println(registerOfCitizens.findCitizensWithSpecificID("99032101238"));
    }
}
