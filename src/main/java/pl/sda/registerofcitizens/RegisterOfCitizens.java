package pl.sda.registerofcitizens;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RegisterOfCitizens {
    HashMap<String ,Citizen> citizenMap=new HashMap<String, Citizen>();

    public void addCitizen(String citizenID, String firstName, String lastName){
        citizenMap.put(citizenID, new Citizen(citizenID,firstName,lastName));
    }

    public List<Citizen> findCitizensBornBefore(int yearToLastDigitsBeforeNewMillenium){
        List<Citizen> listOfCitizens = new ArrayList<Citizen>();
        for (String key:citizenMap.keySet()){
            if (Integer.parseInt(key.substring(0,2))<yearToLastDigitsBeforeNewMillenium){
                listOfCitizens.add(citizenMap.get(key));
            }
        }
        return listOfCitizens;
    }


    public List<Citizen> findCitizensBornBeforeWithName(int yearToLastDigitsBeforeNewMillenium, String name){
        List<Citizen> listOfCitizens = new ArrayList<Citizen>();
        for (String key:citizenMap.keySet()){
            if (Integer.parseInt(key.substring(0,2))<yearToLastDigitsBeforeNewMillenium&&citizenMap.
                    get(key).getFirstName().equals(name)){
                listOfCitizens.add(citizenMap.get(key));
            }
        }
        return listOfCitizens;
    }

    public List<Citizen> findCitizensWithLastName( String lastName){
        List<Citizen> listOfCitizens = new ArrayList<Citizen>();
        for (String key:citizenMap.keySet()){
            if (citizenMap.get(key).getLastName().equals(lastName)){
                listOfCitizens.add(citizenMap.get(key));
            }
        }
        return listOfCitizens;
    }

    public Citizen findCitizensWithSpecificID( String citizenID){

                return citizenMap.get(citizenID);

    }






    }

